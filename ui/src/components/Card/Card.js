// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { StyledCard, Body, Footer, Title } from './Card.styles'

const Card = StyledCard
Card.Body = Body
Card.Footer = Footer
Card.Title = Title

export default Card
